package com.bizmda.bizsip.common;

import cn.hutool.json.JSONObject;

/**
 * 基于平台标准报文的调用接口
 * @author shizhengye
 */
public interface BizMessageInterface {
    /**
     * 服务调用
     * @param inData 传入的平台标准报文
     * @return 返回的平台标准报文
     * @throws BizException Biz-SIP异常
     */
    BizMessage<JSONObject> call(JSONObject inData) throws BizException;
}
