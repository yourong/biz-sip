package com.bizmda.bizsip.common;

import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.Map;
import java.util.concurrent.Callable;

/**
 * @author 史正烨
 */
public class ElThread implements Callable<Object> {
    private final String express;
    private final boolean isBooleanResult;
    private final ExpressionParser parser;
    private final EvaluationContext context;

    public ElThread(String express, Map<String,Object> variableMap, boolean isBooleanResult) {
        this.express = express;
        this.isBooleanResult = isBooleanResult;
        this.parser = new SpelExpressionParser();
        this.context = new StandardEvaluationContext();
        for(Map.Entry<String,Object> entry: variableMap.entrySet()) {
            this.context.setVariable(entry.getKey(),entry.getValue());
        }
    }

    @Override
    public Object call() throws Exception {
        Expression expression = this.parser.parseExpression(this.express, new TemplateParserContext());
        if (this.isBooleanResult) {
            return expression.getValue(this.context, Boolean.class);
        }
        else {
            return expression.getValue(this.context, String.class);
        }
    }
}
