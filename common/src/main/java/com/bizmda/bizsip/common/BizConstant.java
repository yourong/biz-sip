package com.bizmda.bizsip.common;

/**
 * @author shizhengye
 */
public class BizConstant {
    public static final String SERVICE_STATUS_SUCCESS = "0";
    public static final String SERVICE_STATUS_ERROR = "1";
    public static final String SERVICE_STATUS_PROCESSING = "2";

    public static final String NACOS_GROUP="DEFAULT_GROUP";
    public static final String REFRESH_SERVER_ADAPTOR_DATA_ID="bizsip.refresh.server-adaptor";
    public static final String REFRESH_CLIENT_ADAPTOR_DATA_ID="bizsip.refresh.client-adaptor";
    public static final String REFRESH_SERVICE_DATA_ID="bizsip.refresh.service";
    public static final String REFRESH_MESSAGE_DATA_ID="bizsip.refresh.message";

    public static final String DEFAULT_CHARSET_NAME="UTF-8";
    public static final String BIZSIP_LOG_EXCHANGE = "exchange.direct.bizsip.log";
    public static final String BIZSIP_LOG_ROUTING_KEY = "key.bizsip.log";
    public static final String CONTROL_RULE_UPDATING_FLAG = "updatingFlag";

    public static final String APP_SERVICE_EXCHANGE = "exchange.direct.bizsip.app";
    public static final String APP_SERVICE_QUEUE = "queue.bizsip.app";
    public static final String APP_SERVICE_ROUTING_KEY = "key.bizsip.app";

    public static final String RABBITMQ_MESSAGE_HEADER_TRACE_ID = "bizsip-trace-id";

    public static final String APP_SERVICE_SERVICE_ID = "Biz-Service-Id";
    public static final String APP_SERVICE_TRACE_ID = "Biz-Trace-Id";

    private BizConstant() {
    }
}
