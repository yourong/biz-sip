package com.bizmda.bizsip.common;

import lombok.Data;

/**
 * @author shizhengye
 */
@Data
public class DelayAppServiceContext {
    public static final char SERVICE_STATUS_RETRY = '0';
    public static final char SERVICE_STATUS_SUCCESS = '1';
    public static final char SERVICE_STATUS_ERROR = '2';

    private int retryCount = 0;
    private char serviceStatus;
}
