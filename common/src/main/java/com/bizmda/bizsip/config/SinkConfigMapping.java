package com.bizmda.bizsip.config;

import cn.hutool.core.io.resource.ClassPathResource;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import lombok.Getter;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 史正烨
 */
@Getter
public class SinkConfigMapping {
    private final String configPath;
    private Map<String, AbstractSinkConfig> sinkConfigMap;

    public SinkConfigMapping(String configPath) throws BizException {
        this.configPath = configPath;
        this.load();
    }

    public void load() throws BizException {
        Yaml yaml = new Yaml();
        List<Map<String, Object>> sinkMapList;
        try {
            if (this.configPath == null) {
                ClassPathResource resource = new ClassPathResource("/sink.yml");
                sinkMapList = yaml.load(new FileInputStream(resource.getFile()));
            } else {
                sinkMapList = yaml.load(new FileInputStream(this.configPath + "/sink.yml"));
            }
        } catch (FileNotFoundException e) {
            throw new BizException(BizResultEnum.SINK_FILE_NOTFOUND, "sink.yml");
        }
        AbstractSinkConfig sinkConfig;
        this.sinkConfigMap = new HashMap<>(16);
        for (Map<String, Object> sinkMap : sinkMapList) {
            String type = (String) sinkMap.get("type");
            if ("rest".equalsIgnoreCase(type)
                    || "rest-bean".equalsIgnoreCase(type)
                    || "rest-sink-bean".equalsIgnoreCase(type)) {
                sinkConfig = new RestSinkConfig(sinkMap);
            } else if ("rabbitmq".equalsIgnoreCase(type)
                    || "rabbitmq-bean".equalsIgnoreCase(type)
                    || "rabbitmq-sink-bean".equalsIgnoreCase(type)) {
                sinkConfig = new RabbitmqSinkConfig(sinkMap);
            } else {
                continue;
            }
            this.sinkConfigMap.put(sinkConfig.getId(), sinkConfig);
        }
    }

    public AbstractSinkConfig getSinkConfig(String id) throws BizException {
        AbstractSinkConfig sinkConfig = this.sinkConfigMap.get(id);
        if (sinkConfig == null) {
            throw new BizException(BizResultEnum.SINK_NOT_SET,"Sink["+id+"]没有配置，请检查sink.yml!");
        }
        return sinkConfig;
    }
}
