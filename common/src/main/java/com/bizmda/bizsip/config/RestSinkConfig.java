package com.bizmda.bizsip.config;

import cn.hutool.core.text.CharSequenceUtil;
import java.util.Map;

/**
 * @author 史正烨
 */
public class RestSinkConfig extends AbstractSinkConfig {
    private final String url;

    public String getUrl() {
        return url;
    }

    public RestSinkConfig(Map<String,Object> map) {
        super(map);
        this.url = (String)map.get("url");
    }

    @Override
    public String toString() {
        return super.toString()
                + CharSequenceUtil.format(",url={}", this.url);
    }
}
