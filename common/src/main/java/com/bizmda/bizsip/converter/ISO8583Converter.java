package com.bizmda.bizsip.converter;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.converter.iso8583.ByteBitUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import java.nio.charset.StandardCharsets;
import java.util.*;


/**
 * @author 史正烨
 */
@Slf4j
public class ISO8583Converter extends AbstractConverter {
    private static final String FIELD_INDEX = "index";
    private Map<Integer, Map<String,Object>> fieldIndexMap;
    private Map<String, Map<String,Object>> fieldNameMap;
    private String destinationId;
    private String sourceId;

    @Override
    public void init(String configPath, Map<String,Object> messageMap) throws BizException {
        super.init(configPath, messageMap);
        this.destinationId = (String) messageMap.get("destination-id");
        this.destinationId = StrUtil.fillAfter(this.destinationId.trim(), ' ', 11);
        this.sourceId = (String) messageMap.get("source-id");
        this.sourceId = StrUtil.fillAfter(this.sourceId.trim(), ' ', 11);
        List<Map<String,Object>> fields = (List<Map<String,Object>>) messageMap.get("fields");
        this.fieldIndexMap = new HashMap<>(16);
        this.fieldNameMap = new HashMap<>(16);
        String name;
        for (Map<String,Object> field : fields) {
            Object o = field.get(FIELD_INDEX);
            if (o == null) {
                throw new BizException(BizResultEnum.CONVERTOR_ISO8583_COFIG_ERROR);
            }
            int index = (int) field.get(FIELD_INDEX);
            o = field.get("name");

            if (o == null) {
                name = "f" + index;
            } else {
                name = (String) o;
            }
            this.fieldIndexMap.put(index, field);
            this.fieldNameMap.put(name, field);
        }
    }

    @Override
    protected JSONObject biz2json(JSONObject inMessage) {
        return inMessage;
    }

    @Override
    protected byte[] json2adaptor(JSONObject inMessage) throws BizException {
        String bitMapAndMsgStr = getBitMapAndMsg(inMessage, 128);
        int bitMapAndMsgLen = bitMapAndMsgStr.length();
        int rspTotLen = 46 + 4 + bitMapAndMsgLen;

        String head = (String) inMessage.get(this.getMessageHeadFieldName());
        if (head == null) {
            throw new BizException(BizResultEnum.CONVERTOR_ISO8583_PACK_ERROR, "msgHead为空");
        }

        byte[] rspHead = HexUtil.decodeHex(head);
        System.arraycopy(String.format("%04d", rspTotLen).getBytes(), 0, rspHead, 2, 4);
        System.arraycopy(this.destinationId.getBytes(), 0, rspHead, 6, 11);
        System.arraycopy(this.sourceId.getBytes(), 0, rspHead, 17, 11);


        StringBuilder sendMsg = new StringBuilder();
        // 8583消息头
        sendMsg.append(new String(rspHead, StandardCharsets.ISO_8859_1));
        // 8583消息类型
        String messageType = (String) inMessage.get(this.getMessageTypeFieldName());
        if (messageType == null) {
            throw new BizException(BizResultEnum.CONVERTOR_ISO8583_PACK_ERROR, "msgType为空");
        }
        sendMsg.append(messageType);
        // 8583位图和消息
        sendMsg.append(bitMapAndMsgStr);
        return sendMsg.toString().getBytes(StandardCharsets.ISO_8859_1);
    }

    @Override
    protected JSONObject adaptor2json(byte[] receivedMsgBytes) {
        //去掉前面的长度
        int totalLen = receivedMsgBytes.length;
        if (totalLen < 56) {
            log.error("报文格式不正确，报文长度最少为56");
            return null;
        }
        byte[] head = ByteBitUtil.subBytes(receivedMsgBytes, 0, 46);

        String messageType = new String(ByteBitUtil.subBytes(receivedMsgBytes, 46, 4));
        log.trace("MsgType = [" + messageType + "]");

        BitSet bitMap = ByteBitUtil.byteArray2BitSet(ByteBitUtil.subBytes(receivedMsgBytes,50,1));
        byte[] msgBytes;
        if (bitMap.get(0)) {
            bitMap = ByteBitUtil.byteArray2BitSet(ByteBitUtil.subBytes(receivedMsgBytes, 50, 16));
            msgBytes = ByteBitUtil.subBytes(receivedMsgBytes, 46 + 16 + 4, totalLen - (46 + 16 + 4));
        }
        else {
            bitMap = ByteBitUtil.byteArray2BitSet(ByteBitUtil.subBytes(receivedMsgBytes, 50, 8));
            msgBytes = ByteBitUtil.subBytes(receivedMsgBytes, 46 + 8 + 4, totalLen - (46 + 8 + 4));
        }
        JSONObject jsonObject = msgToObject128(bitMap, msgBytes);

        jsonObject.set(this.getMessageTypeFieldName(), messageType);
        jsonObject.set(this.getMessageHeadFieldName(), HexUtil.encodeHexStr(head));
        return jsonObject;
    }

    @Override
    protected JSONObject json2biz(JSONObject inMessage) {
        return inMessage;
    }


    private JSONObject msgToObject128(BitSet bitMap, byte[] msg) {

        int indexFlag = 0;
        String fldName;
        int fldUnfixed;
        int fldLength;
        int dataLength;
        String fldValue;
        JSONObject jsonObject = new JSONObject();
        // 从位图第2位置开始
        for (int i = 1; i < bitMap.length(); i++) {
            if (!bitMap.get(i)) {
                continue;
            }
            // 位图下标从1开始，所以需要+1
            Map<String,Object> fieldDefMap = this.fieldIndexMap.get(i + 1);
            fldName = (String) fieldDefMap.get("name");
            if (fldName == null) {
                fldName = "f" + (i + 1);
            }
            Object o = fieldDefMap.get("unfixed");
            if (o == null) {
                fldUnfixed = 0;
            } else {
                fldUnfixed = (int) o;
            }
            o = fieldDefMap.get("length");
            if (o == null) {
                fldLength = 0;
            } else {
                fldLength = (int) o;
            }

            if (fldUnfixed == 0) {
                dataLength = fldLength;
            } else if (fldUnfixed == 2) {
                dataLength = Integer.parseInt(new String(ByteBitUtil.subBytes(msg, indexFlag, 2)));
                indexFlag = indexFlag + 2;
            } else if (fldUnfixed == 3) {
                dataLength = Integer.parseInt(new String(ByteBitUtil.subBytes(msg, indexFlag, 3)));
                indexFlag = indexFlag + 3;
            } else {
                // 未知类型，不做处理
                continue;
            }
            fldValue = new String(ByteBitUtil.subBytes(msg, indexFlag, dataLength), StandardCharsets.ISO_8859_1);
            indexFlag += dataLength;
            jsonObject.set(fldName, fldValue);
            log.trace("Field " + (i + 1) + "= [" + fldValue + "]");
        }

        return jsonObject;


    }

    private String getBitMapAndMsg(JSONObject jsonObject, int bitLen) throws BizException {
        // 初始化域位图
        BitSet bitMap = new BitSet();
        for (int i = 0; i < 128; i++) {
            bitMap.set(i, false);
        }
        bitMap.set(0, false);

        String fldValue;
        int index;
        // 按照格式处理后的值
        String fldSendValue;
        // 将每个域对应的值，保存到对应下标中
        String[] fldSendValues = new String[bitLen];
        // 循环判断哪个字段有值
        for (String fldName : jsonObject.keySet()) {
            Map<String,Object> field = this.fieldNameMap.get(fldName);
            if (field == null) {
                continue;
            }
            fldValue = jsonObject.getStr(fldName);

            index = (int) field.get(FIELD_INDEX);
            if (CharSequenceUtil.isNotEmpty(fldValue)) {
                // 如果此域有值，将对应的位图位置修改为1
                bitMap.set(index - 1, true);
                // 根据注解对值进行处理
                fldSendValue = verifyAndTransValue(field, fldValue);
                fldSendValues[index - 1] = fldSendValue;
                if (index >= 66) {
                    bitMap.set(0,true);
                }
            }
        }

        // 将128位bitmap转换为16byte
        byte[] bitMapBytes = ByteBitUtil.bitSet2ByteArray(bitMap);
        String bitMapStr;
        bitMapStr = new String(bitMapBytes, StandardCharsets.ISO_8859_1);
        StringBuilder bitMapAndMsg = new StringBuilder();
        // 位图在前，先拼接位图
        bitMapAndMsg.append(bitMapStr);
        // 拼接报文数据
        for (String value : fldSendValues) {
            if (StringUtils.isNotEmpty(value)) {
                bitMapAndMsg.append(value);
            }
        }

        return bitMapAndMsg.toString();
    }

    /**
     * 组包时根据字段原值按照其配置规则转为十六进制 PACK
     */
    private String verifyAndTransValue(Map<String,Object> fieldDefMap, String fldValue) throws BizException {
        int index = (Integer) fieldDefMap.get(FIELD_INDEX);
        String fldName = this.getFieldName(index);
        int fldUnfixed;
        int fldLength;
        Object o = fieldDefMap.get("unfixed");
        if (o == null) {
            fldUnfixed = 0;
        } else {
            fldUnfixed = (int) o;
        }
        o = fieldDefMap.get("length");
        if (o == null) {
            fldLength = 0;
        } else {
            fldLength = (int) o;
        }

        int actualLen = fldValue.length();

        // 固定长度，则校验一下长度是否一致
        if (fldUnfixed == 0) {
            if (actualLen != fldLength) {
                String msg = String.format("%s长度不正确，期望长度为[%d]，实际长度为[%d]。", fldName, fldLength, actualLen);
                log.error(msg);
                throw new BizException(BizResultEnum.CONVERTOR_ISO8583_PACK_ERROR, msg);
            }
            return fldValue;
        }

        // 可变长度，校验一下长度是否超过上限。如果长度符合，则在前边拼接长度值
        if (fldUnfixed == 2 || fldUnfixed == 3) {
            if (actualLen > fldLength) {
                String msg = String.format("%s长度不正确，最大长度为[%d]，实际长度为[%d]。", fldName, fldLength, actualLen);
                throw new BizException(BizResultEnum.CONVERTOR_ISO8583_PACK_ERROR, msg);
            }
            int len = 2;
            if (fldUnfixed == 3) {
                len = 3;
            }
            // 在报文前边拼接长度
            fldValue = StringUtils.leftPad(String.valueOf(actualLen), len, "0") + fldValue;
            return fldValue;
        }
        return fldValue;
    }

    private String getMessageHeadFieldName() {
        if (this.fieldIndexMap.get(0) == null) {
            return "msgHead";
        } else {
            Map<String,Object> fieldDefMap = this.fieldIndexMap.get(0);
            return (String) fieldDefMap.get("name");
        }
    }

    private String getMessageTypeFieldName() {
        if (this.fieldIndexMap.get(1) == null) {
            return "msgType";
        } else {
            Map<String,Object> fieldDefMap = this.fieldIndexMap.get(1);
            return (String) fieldDefMap.get("name");
        }
    }

    private String getFieldName(int index) throws BizException {
        Map<String,Object> fieldMap = this.fieldIndexMap.get(index);
        if (fieldMap == null) {
            throw new BizException(BizResultEnum.CONVERTOR_ISO8583_COFIG_ERROR);
        }
        String name = (String) fieldMap.get("name");
        if (name == null) {
            return "f" + index;
        }
        return name;
    }
}
