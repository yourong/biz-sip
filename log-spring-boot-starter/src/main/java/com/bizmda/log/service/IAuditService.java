package com.bizmda.log.service;

import com.bizmda.log.model.Audit;

/**
 * 审计日志接口
 *
 * @author zlt
 * Blog: https://zlt2000.gitee.io
 * Github: https://github.com/zlt2000
 */
public interface IAuditService {
    /**
     * 审计日志保存
     * @param audit 审计信息
     */
    void save(Audit audit);
}
