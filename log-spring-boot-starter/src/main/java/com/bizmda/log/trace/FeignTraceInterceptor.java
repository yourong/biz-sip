package com.bizmda.log.trace;

import cn.hutool.core.text.CharSequenceUtil;
import feign.RequestInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;

/**
 * @author shizhengye
 */
@ConditionalOnClass(value = {RequestInterceptor.class})
public class FeignTraceInterceptor {

    @Bean
    public RequestInterceptor feignTraceInterceptor() {
        return template -> {
                //传递日志traceId
                String traceId = MDCTraceUtils.getTraceId();
                if (!CharSequenceUtil.isEmpty(traceId)) {
                    template.header(MDCTraceUtils.TRACE_ID_HEADER, traceId);
                }
        };
    }
}
