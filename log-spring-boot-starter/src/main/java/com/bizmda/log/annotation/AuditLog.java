package com.bizmda.log.annotation;

import java.lang.annotation.*;

/**
 * @author zlt
 * Blog: https://zlt2000.gitee.io
 * Github: https://github.com/zlt2000
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuditLog {
    /**
     * 操作信息
     * @return 操作串
     */
    String operation();
}
