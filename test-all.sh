#!/bin/sh
set -v on
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/script/sample1" -X POST --data '{"accountNo":"62001818","sex":"0","email":"123232@163.com","mobile":"18601872345"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample1" -X POST --data '{"accountNo":"62001818","sex":"0","email":"123232@163.com","mobile":"18601872345"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/script/sample2" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample2" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -X POST --data '{"accountNo":"003"}' http://localhost:8080/source1|jq
curl http://localhost:8080/hello?message=world
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/simple-xml-sink" -X POST --data '{"accountName": "王五","balance": 500,"accountNo":"005"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/velocity-json-sink" -X POST --data '{"accountName": "王五","sex": "0","accountNo":"005"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/velocity-json-sink" -X POST --data '{"accountName": "王五","sex": "1","accountNo":"005"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/velocity-json-sink" -X POST --data '{"accountName": "王五","sex": "2","accountNo":"005"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/velocity-xml-sink" -X POST --data '{"accountName": "王五","sex": "0","accountNo":"005","balance":100}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/velocity-xml-sink" -X POST --data '{"accountName": "王五","sex": "1","accountNo":"005","balance":1000}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/velocity-xml-sink" -X POST --data '{"accountName": "王五","sex": "2","accountNo":"005","balance":1000}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/fixed-length-sink" -X POST --data '{"accountName": "王五","balance": 500,"accountNo":"005","sex":"0"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/velocity-split-sink" -X POST --data '{"accountName": "王五","balance": 500,"accountNo":"005","sex":"0"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/iso-8583-sink" -X POST --data '{"msgType": "0800","reserved60": "000000000030","card_accptr_id42": "898411341310014","systemTraceAuditNumber11": "000001","switching_data62": "53657175656e6365204e6f3132333036303733373832323134","card_accptr_termnl_id41": "73782214","msgHead": "31323334353637383930313233343536373839303132333435363738393031323334353637383930313233343536","acct_id_1_102": "1234567890","fin_net_data63": "303031"}' http://localhost:8888/api|jq
#echo '{"accountName": "王五","balance": 500,"accountNo":"xxx"}'|nc -l 10010
#curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/netty-connector-sink" -X POST --data '{"accountNo":"999"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/rabbitmq-connector-sink" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/http-connector-sink" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/rabbitmq-hello" -X POST --data '{"methodName":"hello","params":["world"]}' http://localhost:8888/api|jq

curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample1-check-rule" -X POST --data '{"accountNo":"62001818","sex":"0","email":"123232@163.com","mobile":"18601872345"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample1-check-rule" -X POST --data '{"accountNo":"62001818","sex":"0","email":"123232","mobile":"18601872345"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample1-check-rule" -X POST --data '{"accountNo":"62001818","sex":"0","email":"123232@163.com","mobile":"021-34554345"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample1-check-rule" -X POST --data '{"accountNo":"62001818","email":"123232@163.com","mobile":"18601872345"}' http://localhost:8888/api|jq
echo '{"serviceId":"/script/sample2","accountNo":"003"}'|nc localhost 10020
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/script/sample10" -X POST --data '{"accountNo":"002"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/script/sample11" -X POST --data '{"accountNo":"002"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/script/sample12" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample13" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample14" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample15" -X POST --data '{"maxRetryCount":2,"result":"success"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample16" -X POST --data '{"methodName":"doService1","params":["003"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample16" -X POST --data '{"methodName":"doService2","params":["003",1]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample16" -X POST --data '{"methodName":"queryCustomerDTO","params":["003"]}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/hello-sink" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/sample17-sink" -X POST --data '{"accountName": "王五","sex": "0","accountNo":"005","balance":100}' http://localhost:8888/api|jq
curl -H "Content-Type:application/xml" -X POST --data '<?xml version="1.0" encoding="UTF-8" standalone="no"?><root><accountName>王五</accountName><balance>500</balance><accountNo>005</accountNo></root>' http://localhost:8080/source4
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/sink/rabbitmq-sink" -X POST --data '{"accountName": "王五","sex": "0","accountNo":"005","balance":100}' http://localhost:8888/api|jq
#curl -H "Content-Type:application/json" -H "Biz-Service-Id:/script/sample13" -X POST --data '{"accountNo":"62001818","sex":"0","email":"123232@163.com","mobile":"18601872345","key":"001","amount":1}' http://localhost:8888/api|jq
#curl -H "Content-Type:application/json" -H "Biz-Service-Id:/script/sample13" -X POST --data '{"accountNo":"62001818","sex":"0","email":"123232@163.com","mobile":"18601872345","key":"001","amount":1,"delete":1}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/sample1-control-rule" -X POST --data '{"tran_code":"0001","account":"001","amount":500,"other_account":"002"}' http://localhost:8888/api|jq
curl -H "Content-Type:application/json" -H "Biz-Service-Id:/bean/long-tcp-netty" -X POST --data '{"accountNo":"003"}' http://localhost:8888/api|jq
curl http://localhost:8080/add?message=999
curl http://localhost:8080/breaker?a=5
