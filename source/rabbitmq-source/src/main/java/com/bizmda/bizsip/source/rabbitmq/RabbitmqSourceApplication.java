package com.bizmda.bizsip.source.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@Slf4j
@SpringBootApplication
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class RabbitmqSourceApplication {
    public static void main(String[] args) {
        SpringApplication.run(RabbitmqSourceApplication.class, args);
    }
}
