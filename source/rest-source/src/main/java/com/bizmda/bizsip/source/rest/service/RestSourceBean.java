package com.bizmda.bizsip.source.rest.service;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizMessageInterface;
import com.bizmda.bizsip.source.api.SourceBeanInterface;
import com.bizmda.bizsip.source.api.SourceClientFactory;
import com.bizmda.bizsip.source.rest.controller.RestSourceDTO;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shizhengye
 */
@Service
public class RestSourceBean implements SourceBeanInterface {
    private final Map<String, BizMessageInterface> appServiceMap = new HashMap<>();
    private final BizMessageInterface errorAppService = SourceClientFactory.getAppServiceClient(BizMessageInterface.class, "/source1/error");

    @Override
    public Object process(Object data) throws BizException {
        BizMessage<JSONObject> bizMessage;
        RestSourceDTO restSourceDTO = (RestSourceDTO) data;
        Map<String,String> headerMap = restSourceDTO.getHeaderMap();
        JSONObject jsonObject = restSourceDTO.getJsonObjectData();
        String serviceId = headerMap.get("service-id");
        if (CharSequenceUtil.isEmpty(serviceId)) {
            bizMessage = this.errorAppService.call(jsonObject);
            return bizMessage.getData();
        }
        BizMessageInterface appService = this.appServiceMap.computeIfAbsent(serviceId,
                key -> SourceClientFactory.getAppServiceClient(BizMessageInterface.class, key));
        bizMessage = appService.call(jsonObject);
        return bizMessage.getData();
    }
}
