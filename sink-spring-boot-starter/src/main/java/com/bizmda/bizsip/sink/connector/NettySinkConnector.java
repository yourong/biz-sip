package com.bizmda.bizsip.sink.connector;

import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.sink.connector.netty.NettyClient;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 史正烨
 */
@Slf4j
public class NettySinkConnector extends AbstractSinkConnector implements ByteProcessInterface {
    private NettyClient nettyClient;

    @Override
    public void init(AbstractSinkConfig sinkConfig) throws BizException {
        super.init(sinkConfig);
        String host = (String) sinkConfig.getConnectorMap().get("host");
        Integer port = (Integer) sinkConfig.getConnectorMap().get("port");
        log.info("初始化NettySinkConnector:地址[{}]，端口[{}]",host,port);
        this.nettyClient = new NettyClient(host,port);
    }

    @Override
    public byte[] process(byte[] inMessage) throws BizException {
        log.debug("调用process()");
        return this.nettyClient.call(inMessage);
    }
}
