package com.bizmda.bizsip.sink.connector;

import com.bizmda.bizsip.common.BizException;

/**
 * @author shizhengye
 */
public interface ByteProcessInterface {
    /**
     * service类型Connector类，要实现的基于字节流byte[]作为出入参数的接口
     * @param inbytes 输入字节流
     * @return 输出字节流
     * @throws BizException BizException异常
     */
    byte[] process(byte[] inbytes) throws BizException;
}
