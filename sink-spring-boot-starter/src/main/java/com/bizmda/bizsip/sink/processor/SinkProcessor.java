package com.bizmda.bizsip.sink.processor;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shizhengye
 */
@Slf4j
public class SinkProcessor extends AbstractSinkProcessor {
    public SinkProcessor(AbstractSinkConfig sinkConfig) {
        super(sinkConfig);
    }

    @Override
    JSONObject process(JSONObject inMessage) throws BizException {
        log.debug("调用Sink服务[{}](处理器类型default)",
                this.sinkConfig.getId());
        log.trace("Sink服务请求报文:\n{}", BizUtils.buildJsonLog(inMessage));

        log.debug("调用Converter[{}]打包", this.converter.getConverter().getType());
        byte[] packedMessage = this.converter.pack(inMessage);
        log.trace("Converter打包响应报文:\n{}", BizUtils.buildHexLog(packedMessage));
        log.debug("调用Sink Connector[{}]通讯交互", this.connector.getSinkConnector().getType());
        byte[] returnMessage = this.connector.process(packedMessage);
        log.trace("Sink Connector响应报文:\n{}", BizUtils.buildHexLog(returnMessage));
        log.debug("调用Converter[{}]解包", this.converter.getConverter().getType());
        JSONObject unpackedJsonObject = this.converter.unpack(returnMessage);
        log.trace("Converter解包响应报文:\n{}", BizUtils.buildJsonLog(unpackedJsonObject));
        return unpackedJsonObject;
    }
}
