package com.bizmda.bizsip.app.checkrule;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author shizhengye
 */
@Data
public class CheckRuleConfig {
    private static final String CHECK_MODE_ALL = "all";
    private CheckMode fieldCheckMode = CheckMode.ONE;
    private List<FieldCheckRule> fieldCheckRuleList;
    private CheckMode serviceCheckMode = CheckMode.ONE;
    private List<ServiceCheckRule> serviceCheckRuleList;

    public CheckRuleConfig(Map<String,Object> checkRuleConfigmap) {
        String mode = (String)checkRuleConfigmap.get("field-check-mode");
        if(CHECK_MODE_ALL.equalsIgnoreCase(mode)) {
            this.fieldCheckMode = CheckMode.ALL;
        }
        mode = (String)checkRuleConfigmap.get("service-check-mode");
        if(CHECK_MODE_ALL.equalsIgnoreCase(mode)) {
            this.serviceCheckMode = CheckMode.ALL;
        }

        List<Map<String,Object>> mapList = (List<Map<String,Object>>)checkRuleConfigmap.get("field-check-rules");
        if (mapList == null) {
            mapList = new ArrayList<>();
        }
        this.fieldCheckRuleList = new ArrayList<>();
        for(Map<String,Object> map:mapList) {
            FieldCheckRule fieldCheckRule = new FieldCheckRule(map);
            this.fieldCheckRuleList.add(fieldCheckRule);
        }

        mapList = (List<Map<String,Object>>)checkRuleConfigmap.get("service-check-rules");
        if (mapList == null) {
            mapList = new ArrayList<>();
        }
        this.serviceCheckRuleList = new ArrayList<>();
        for(Map<String,Object> map:mapList) {
            ServiceCheckRule serviceCheckRule = new ServiceCheckRule(map);
            this.serviceCheckRuleList.add(serviceCheckRule);
        }
    }
}
