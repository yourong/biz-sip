package com.bizmda.bizsip.app.controlrule;

import lombok.Data;

import java.util.Map;

/**
 * @author shizhengye
 */
@Data
public class ControlRule {
    private String script;
    private String name;

    public ControlRule(Map<String,Object> map) {
        this.name = (String)map.get("name");
        String myScript = (String)map.get("script");
        if (myScript != null) {
            this.script = myScript;
        }
    }
}
