package com.bizmda.bizsip.app.config;

import com.bizmda.bizsip.service.AppLogService;
import com.bizmda.bizsip.common.BizConstant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author shizhengye
 */
@Slf4j
@Configuration
public class RabbitmqConfig {

    public static final String DELAY_SERVICE_EXCHANGE = "exchange.direct.bizsip.delayservice";
    public static final String DELAY_SERVICE_QUEUE = "queue.delay.bizsip.delayservice";
    public static final String DELAY_SERVICE_ROUTING_KEY = "key.bizsip.delayservice";
    @Value("${bizsip.rabbitmq-log:#{null}}")
    private String rabbitmqLog;

    @Autowired
    private CachingConnectionFactory connectionFactory;

    @Bean
    public DirectExchange logExchange(){
        return new DirectExchange(BizConstant.BIZSIP_LOG_EXCHANGE, true, false);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(){
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public AppLogService appLogService() {
        return new AppLogService(this.rabbitTemplate(),this.rabbitmqLog);
    }
}
