package com.bizmda.bizsip.app.controlrule;

import lombok.Data;

import java.io.Serializable;

/**
 * @author shizhengye
 */
@Data
public class ControlRuleMetric implements Serializable {
    private double score = 0;
}
