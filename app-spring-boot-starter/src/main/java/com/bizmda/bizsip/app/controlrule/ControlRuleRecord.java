package com.bizmda.bizsip.app.controlrule;

import lombok.Data;
import java.io.Serializable;

/**
 * @author shizhengye
 */
@Data
public class ControlRuleRecord implements Serializable {
    private long time;
    private double score;
    private String key2;
}
