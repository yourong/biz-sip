package com.bizmda.bizsip.app.checkrule;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.controlrule.ControlRule;
import com.bizmda.bizsip.app.executor.script.MagicScriptHelper;
import org.ssssssss.script.MagicScriptContext;

import java.util.concurrent.Callable;

/**
 * @author 史正烨
 */
public class ControlRuleThread implements Callable<ControlRuleResult> {
    private final int index;
    private final ControlRule controlRule;
    private final JSONObject jsonObject;

    public ControlRuleThread(int index,JSONObject jsonObject, ControlRule controlRule) {
        this.index = index;
        this.controlRule = controlRule;
        this.jsonObject = jsonObject;
    }

    @Override
    public ControlRuleResult call() {
        MagicScriptContext context = new MagicScriptContext();
        context.set("request", this.jsonObject);
        Object result = MagicScriptHelper.executeScript(this.controlRule.getScript(), context);
        return ControlRuleResult.builder()
                .index(this.index).result(result).name(this.controlRule.getName())
                .build();
    }
}
