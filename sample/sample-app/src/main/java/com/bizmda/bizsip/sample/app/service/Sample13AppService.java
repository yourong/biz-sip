package com.bizmda.bizsip.sample.app.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessageInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author shizhengye
 */
@Slf4j
@Service
public class Sample13AppService implements AppBeanInterface {
    private final BizMessageInterface delayBizServiceClient = AppClientFactory
            .getDelayAppServiceClient(BizMessageInterface.class,"/script/sample12-1",1000,2000,4000);

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        this.delayBizServiceClient.call(message);
        return message;
    }
}
