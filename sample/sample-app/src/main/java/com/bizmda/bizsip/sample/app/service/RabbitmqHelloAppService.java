package com.bizmda.bizsip.sample.app.service;

import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.sample.sink.api.HelloInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author shizhengye
 */
@Slf4j
@Service
public class RabbitmqHelloAppService implements HelloInterface {
    private final HelloInterface restHelloInterface =  AppClientFactory.getSinkClient(HelloInterface.class,"hello-bean-sink");
    private final HelloInterface rabbitmqHelloInterface =  AppClientFactory.getSinkClient(HelloInterface.class,"rabbitmq-hello-bean-sink");
    @Override
    public String hello(String message) {
        log.info("1:{}",this.restHelloInterface.hello("1"));
        log.info("2:{}",this.rabbitmqHelloInterface.hello("2"));
        log.info("3:{}",this.restHelloInterface.hello("3"));
        log.info("4:{}",this.rabbitmqHelloInterface.hello("4"));
        log.info("5:{}",this.restHelloInterface.hello("5"));
        log.info("6:{}",this.rabbitmqHelloInterface.hello("6"));
        return "hello," + message;
    }

    @Override
    public BigDecimal addBigDecimal(BigDecimal bigDecimal) {
        return bigDecimal.add(new BigDecimal("1.0001"));
    }

    @Override
    public void doCircuitBreakerService(int a) throws BizException {
        this.restHelloInterface.doCircuitBreakerService(a);
        this.rabbitmqHelloInterface.doCircuitBreakerService(a);
    }
}
