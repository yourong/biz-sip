package com.bizmda.bizsip.sample.app.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizMessageInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author shizhengye
 */
@Slf4j
@Service
public class Sample15AppService implements AppBeanInterface {
    private final BizMessageInterface bizMessageInterface
            = AppClientFactory.getDelayAppServiceClient(
            BizMessageInterface.class, "/bean/sample15-delay",
            0,1000, 2000, 4000, 8000, 16000);

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        BizMessage<JSONObject> bizMessage = this.bizMessageInterface.call(message);
        return bizMessage.getData();
    }
}
