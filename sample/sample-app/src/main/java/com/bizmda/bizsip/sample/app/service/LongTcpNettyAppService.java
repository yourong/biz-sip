package com.bizmda.bizsip.sample.app.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizMessageInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author shizhengye
 */
@Slf4j
@Service
public class LongTcpNettyAppService implements AppBeanInterface {
    private final BizMessageInterface sink1Interface = AppClientFactory
            .getSinkClient(BizMessageInterface.class,"long-tcp-netty-sink");

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        BizMessage<JSONObject> bizMessage = this.sink1Interface.call(message);
        return bizMessage.getData();
    }
}
