package com.bizmda.bizsip.sample.source.longtcpnetty;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * @author: 史正烨
 * @date: 2022/3/1 11:42 上午
 * @Description:
 */
public class ClientChannelInitializer extends ChannelInitializer<Channel> {
    @Override
    protected void initChannel(Channel ch) {
        ch.pipeline()
                //10 秒没发送消息 将IdleStateHandler 添加到 ChannelPipeline 中
                .addLast(new IdleStateHandler(0, 5, 0))
                .addLast(new SampleEncoder())
                .addLast(new NettyClientHandler());
    }
}