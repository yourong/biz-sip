package com.bizmda.bizsip.sample.source.controller;

import com.bizmda.bizsip.common.BizCircuitBreaker;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.sample.sink.api.HelloInterface;
import com.bizmda.bizsip.source.api.SourceClientFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;

/**
 * @author 史正烨
 */
@Slf4j
@Controller
public class HelloSourceController {
    private final HelloInterface helloInterface = SourceClientFactory
            .getAppServiceClient(HelloInterface.class,"/bean/hello");

    @GetMapping(value = "/hello")
    @ResponseBody
    public String doService(String message) throws BizException {
        String traceId = SourceClientFactory.createTraceId();
        log.info("traceId:{}",traceId);
        String result = this.helloInterface.hello(message);
        SourceClientFactory.removeTraceId();
        return result;
    }

    @GetMapping(value = "/add")
    @ResponseBody
    public BigDecimal addBigDecimal(String message) throws BizException {
        BigDecimal bigDecimal = new BigDecimal(message);
        bigDecimal = this.helloInterface.addBigDecimal(bigDecimal);
        return bigDecimal;
    }

    @GetMapping(value = "/breaker")
    @ResponseBody
    public void addBigDecimal(int a) throws BizException {
        log.info("调用前Sink熔断器状态:\n{}",BizCircuitBreaker.buildTrace());
        BizCircuitBreaker.willCallSinks("hello-bean-sink");
        this.helloInterface.doCircuitBreakerService(a);
        log.info("调用后Sink熔断器状态:\n{}",BizCircuitBreaker.buildTrace());
    }
}