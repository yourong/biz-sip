package com.bizmda.bizsip.sample.source.longtcpnetty;

import lombok.Builder;
import lombok.Getter;

/**
 * @author: 史正烨
 * @date: 2022/3/1 10:34 上午
 * @Description:
 */
@Builder
@Getter
public class SampleMessage {
    private short length;
    private byte[] data;

}
