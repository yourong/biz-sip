package com.bizmda.bizsip.sample.sink.service;

import com.bizmda.bizsip.common.BizCircuitBreaker;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.sample.sink.api.HelloInterface;
import com.bizmda.bizsip.sink.api.AbstractSinkService;
import com.bizmda.bizsip.sink.api.SinkCircuitBreaker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * @author 史正烨
 */
@Slf4j
@Service
public class HelloBeanSinkService extends AbstractSinkService implements HelloInterface {
    @Override
    public String hello(String message) {
        log.info("hello({})",message);
        return "hello,"+message;
    }

    @Override
    public BigDecimal addBigDecimal(BigDecimal bigDecimal) {
        log.info("addBigDecimal({})",bigDecimal);
        return bigDecimal.add(new BigDecimal("1.0001"));
    }

    private static int i = 0;
    @Override
    public void doCircuitBreakerService(int a) throws BizException {
        int b = i % a;
        if (b == 0) {
            SinkCircuitBreaker.success();
            log.info("Sink circuit breaker: success!");
        }
        else {
            SinkCircuitBreaker.error();
            log.info("Sink circuit breaker: error!");
        }
        i++;
    }

}
