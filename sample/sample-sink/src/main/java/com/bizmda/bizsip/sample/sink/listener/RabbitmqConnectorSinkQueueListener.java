package com.bizmda.bizsip.sample.sink.listener;

import com.bizmda.bizsip.common.BizUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * RabbitMQ接收服务
 * @author shizhengye
 */
@Slf4j
@Service
public class RabbitmqConnectorSinkQueueListener {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "queue.bizsip.rabbitmq-connector-sink", durable = "true", autoDelete = "false"),
            exchange = @Exchange(value = "exchange.direct.bizsip.sink", type = ExchangeTypes.DIRECT, durable = "true", autoDelete = "false"),
            key = "key.bizsip.rabbitmq-connector-sink"))
    public void onMessage(Message inMessage) {
        byte[] bytes = inMessage.getBody();
        log.info("收到报文:\n{}",BizUtils.buildHexLog(bytes));

        Message outMessage = MessageBuilder.withBody(bytes)
                    .setCorrelationId(inMessage.getMessageProperties().getCorrelationId())
                    .build();
            rabbitTemplate.send("",
                    inMessage.getMessageProperties().getReplyTo(),
                    outMessage);
    }
}
