package com.bizmda.bizsip.sample.sink.longtcpnetty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @author: 史正烨
 * @date: 2022/3/1 1:59 下午
 * @Description:
 */
public class SampleEncoder extends MessageToByteEncoder<SampleMessage> {
    @Override
    protected void encode(ChannelHandlerContext ctx, SampleMessage msg, ByteBuf out) {
        out.writeShort(msg.getLength());
        out.writeBytes(msg.getData()) ;
    }

}
