package com.bizmda.bizsip.sample.sink.api;

import com.bizmda.bizsip.common.BizException;

import java.math.BigDecimal;

/**
 * @author shizhengye
 */
public interface HelloInterface {
    /**
     * hello方法
     * @param message 传入值
     * @return hello返回值
     */
    String hello(String message);

    BigDecimal addBigDecimal(BigDecimal bigDecimal);

    void doCircuitBreakerService(int a) throws BizException;
}
