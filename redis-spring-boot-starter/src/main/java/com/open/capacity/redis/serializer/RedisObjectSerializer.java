package com.open.capacity.redis.serializer;

import org.springframework.core.convert.converter.Converter;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * @author 作者 owen
 * @version 创建时间：2017年04月23日 下午20:01:06 类说明
 * 类说明
 */
public class RedisObjectSerializer implements RedisSerializer<Object> {
	private final Converter<Object, byte[]> serializingConverter = new SerializingConverter();
	private final Converter<byte[], Object> deserializingConverter = new DeserializingConverter();
	private static final byte[] EMPTY_BYTE_ARRAY = new byte[0];

	@Override
	public byte[] serialize(Object obj) throws SerializationException {
		if (obj == null) {
			// 这个时候没有要序列化的对象出现，所以返回的字节数组应该就是一个空数组
			return EMPTY_BYTE_ARRAY;
		}
		// 将对象变为字节数组
		return this.serializingConverter.convert(obj);
	}

	@Override
	public Object deserialize(byte[] data) throws SerializationException {
		if (data == null || data.length == 0) {
			// 此时没有对象的内容信息
			return null;
		}
		return this.deserializingConverter.convert(data);
	}

}