package com.open.capacity.redis.util;

 
/**
 * @author shizhengye
 */
public interface RedisSubscribeCallback {
    void callback(String msg);
}
