docker_registry="dockerhub.qingcloud.com/bizsip_istio"
echo "build integrator..."
cp integrator/src/main/docker/Dockerfile integrator/target
docker build -t ${docker_registry}/bizsip-integrator integrator/target
docker push ${docker_registry}/bizsip-integrator
echo "build sample-sink..."
cp sample/sample-sink/src/main/docker/Dockerfile sample/sample-sink/target
docker build -t ${docker_registry}/bizsip-sample-sink sample/sample-sink/target
docker push ${docker_registry}/bizsip-sample-sink
echo "build sample-source..."
cp sample/sample-source/src/main/docker/Dockerfile sample/sample-source/target
docker build -t ${docker_registry}/bizsip-sample-source sample/sample-source/target
docker push ${docker_registry}/bizsip-sample-source
echo "build netty-source..."
cp source/netty-source/src/main/docker/Dockerfile source/netty-source/target
docker build -t ${docker_registry}/bizsip-netty-source source/netty-source/target
docker push ${docker_registry}/bizsip-netty-source
echo "build rest-source..."
cp source/rest-source/src/main/docker/Dockerfile source/rest-source/target
docker build -t ${docker_registry}/bizsip-rest-source source/rest-source/target
docker push ${docker_registry}/bizsip-rest-source